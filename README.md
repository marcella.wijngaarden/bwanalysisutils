
This repository contains code snippets that I import in multiple places for analysing BayesWave runs. Some notes so I don't forget.

### Use on CIT/Personal computers

Add path to repository to systems `PYTHONPATH`. After that can use `import gwutils`.

On CIT:

```
export PYTHONPATH=$PYTHONPATH:/home/marcella.wijngaarden/Projects/AnalysisUtils
```

### Use in CIT Jupter Notebook hub

Append repo directory to path in notebook before doing imports from `gwutils`:

```
import sys
sys.path.append('/home/marcella.wijngaarden/Projects/AnalysisUtils')

# Now an do any imports, e.g.,
from gwutils.utils import lambdasToLambdaT
```

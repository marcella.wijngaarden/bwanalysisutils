import numpy as np

def lambdasToLambdaT(m1, m2, lambda1, lambda2):
    """
    Returns LambdaT for given compenent masses and lambdas.
    From Chatziioannou (2020), Eq. 8, http://arxiv.org/abs/2006.03168
    """
    L1 = (lambda1/(m1**5))
    L2 = (lambda2/(m2**5))
    return (16/3.)*(((m1+12*m2)*((m1**4) * L1))+(m2 +12*m1)*((m2**4) * L2))/((m1 + m2)**5)


def NRTunedTidesComputeKappa2T(
        m1,      #  Mass of companion 1 Msun */
        m2,      #  Mass of companion 2 (Msun) */
        lambda1, # (tidal deformability of mass 1) / m1^5 (dimensionless) */
        lambda2  # (tidal deformability of mass 2) / m2^5 (dimensionless) */
    ):

    """
     * convienient function to compute tidal coupling constant. Eq. 2 in arXiv:1706.02969
     * given masses and lambda numbers
    """

    # const double m1 = m1_SI / MSUN_SI;
    # const double m2 = m2_SI / MSUN_SI;
    mtot = m1 + m2

    # /* Xa and Xb are the masses normalised for a total mass = 1 */
    # /* not the masses appear symmetrically so we don't need to switch them. */
    Xa = m1 / mtot
    Xb = m2 / mtot

    # /**< tidal coupling constant. This is the
    #   kappa^T_eff = 2/13 [  (1 + 12 X_B/X_A) (X_A/C_A)^5 k^A_2 +  [A <- -> B]  ]
    #   from Tim Dietrich */

    # /* Note that 2*k_2^A/c_A^5 = 3*lambda1 */
    term1 = ( 1.0 + 12.0*Xb/Xa ) * (Xa**5.0) * lambda1
    term2 = ( 1.0 + 12.0*Xa/Xb ) * (Xb**5.0) * lambda2
    kappa2T = (3.0/13.0) * ( term1 + term2 )

    return kappa2T


# /**
#  * compute the merger frequency of a BNS system.
#  * Tim's new fit that incorporates mass-ratio and asymptotes to zero for large kappa2T.
#  */
def NRTunedTidesMergerFrequency(
        mtot_MSUN, # /**< total mass of system (solar masses) */
        kappa2T,   # /**< tidal coupling constant. Eq. 2 in arXiv:1706.02969 */
        q          #/**< mass-ratio q >= 1 */
    ):

    MTSUN_SI = 4.925491025543575903411922162094833998e-6

    a_0 = 0.3586
    n_1 = 3.35411203e-2
    n_2 = 4.31460284e-5
    d_1 = 7.54224145e-2
    d_2 = 2.23626859e-4

    kappa2T2 = kappa2T * kappa2T

    num = 1.0 + n_1*kappa2T + n_2*kappa2T2
    den = 1.0 + d_1*kappa2T + d_2*kappa2T2
    Q_0 = a_0 / np.sqrt(q)

    # /* dimensionless angular frequency of merger */
    Momega_merger = Q_0 * (num / den)

    # /* convert from angular frequency to frequency (divide by 2*pi)
    #  * and then convert from
    #  * dimensionless frequency to Hz (divide by mtot * LAL_MTSUN_SI)
    #  */
    fHz_merger = Momega_merger / (2*np.pi) / (mtot_MSUN * MTSUN_SI)

    return fHz_merger

def getMergerFrequency(Mtot, m1, m2, lambda1, lambda2):

    kappa2T = NRTunedTidesComputeKappa2T(m1, m2, lambda1, lambda2)
    q = m2/m1

    fgw = NRTunedTidesMergerFrequency(Mtot, kappa2T, q)

    return fgw

def kT_eff(lambdaT):

    # XA = m1/M
    # XB = m2/M 
    # CA = 

    # kt = (2/13.) * (k2A * (1+12*XB/XA) * (XA/CA)**5 + k2B * (1+12*XA/XB) * (XB/CB)**5 )

    return 16/3

def computeMergerFreq():
    """
    Eq. 11 from https://arxiv.org/pdf/1804.02235.pdf
    """
    omega0 = 0.3586
    n1 = 3.354e-2
    n2 = 4.315e-5
    d1 = 7.542e-2
    d2 = 2.236e-4

    omega_factor = (1+n1*kt2+n2*(kt2)**2)/(1+d1*kt2+d2*(kt2)**2)

    omega =  omega0 * np.sqrt(XB/XA) * omega_factor
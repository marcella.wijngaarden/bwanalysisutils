import sys
import h5py
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import matplotlib 
import scipy.stats

# BayesWave output columns of the cbc parametes
COLUMN_CODES = [
    "m1_source", "m2_source", "Mchirp_source", "Mtot_source", 
    "a1", "a2", "chieff",
    "phic", "tc",
    "Dl", "z", 
    "RA", "sindec", "psi", "ciota",
    "m1_det", "m2_det", "Mchirp_det", "Mtot_det",
]
COLUMN_CODES += ["lambda1", "lambda2", "lambdaT", "dLambdaT", "q"]

colors = ["#ED5564", "#FFCE54", "#A0D568", "#4FC1E8", "#AC92EB"]

MODEL_COLORS = {"cbc": colors[0], "cbcsignal": colors[3], "signal": colors[2]}

# Dictionary that maps column codes to verbose/formatted labels
AX_LABELS = {
    'Mchirp_source': r'$\cal{M}$  $(M_{\odot})$', 
    'Mtot_source': r'$M_{tot}$ $(M_{\odot})$', 
    'Mchirp_det': r'$\cal{M}$  $(M_{\odot})$', 
    'Mtot_det': r'$M_{tot}$ $(M_{\odot})$', 
    'chieff': r'$\chi_{\mathrm{eff}$',
    'lambdaT': r'$\tilde{\Lambda}$',
}

# Generic default styles
matplotlib.rcParams['figure.figsize'] = (8.08, 5.)
matplotlib.rcParams['xtick.labelsize'] = 20.0
matplotlib.rcParams['ytick.labelsize'] = 20.0
matplotlib.rcParams['axes.labelsize'] = 20 # 26.0
matplotlib.rcParams['legend.fontsize'] = 16 # 13
# matplotlib.rcParams['font.family']= 'Times New Roman'
# matplotlib.rcParams['font.sans-serif']= ['Bitstream Vera Sans']
# matplotlib.rcParams['text.usetex']= True
# matplotlib.rcParams['mathtext.fontset']= 'stixsans'

matplotlib.rc('font',**{'family':'serif','sans-serif':['Avant Garde']})


# https://maketintsandshades.com/#ED5564  
# https://maketintsandshades.com/#ED5564  
COLOR_SHADES = {
    "#ED5564": ["#fdeef0", "#ed5564"], # colors[0]
    "#FFCE54": ["#fff5dd", "#ffce54"], # colors[1]
    "#A0D568": ["#ecf7e1", "#a0d568"], # colors[2]
    "#4FC1E8": ["#edf9fd", "#204d5d"], # colors[3]
    "k": ["#b3b3b3", "k"]
}

def make_corner_plot(dataset, plot_params=[], figsize=(15,15), nbins=300, outputfile="", ax_labels=AX_LABELS, mpl_rcparams=None, plot_legend=None,
    fill=True):
    """
    Return a corner plot for chosen CBC parameters.
    """
    
    plt.rc('text', usetex=True)

    indices = {}
    for plot_param in plot_params:
        indices[plot_param] = COLUMN_CODES.index(plot_param)
    
    # Set defaults for plotting the legend in case param not given
    if plot_legend == None:
        if len(dataset) > 1:
            plot_legend = True
        else:
            plot_legend = False
            
    # Possible to override matplotlib rc params
    if mpl_rcparams:
        matplotlib.rcParams = mpl_rcparams

    N = len(plot_params)
    fig, axes = plt.subplots(N, N, figsize=figsize)
    legend_handles = []


    # ylims = {}
    # for j, xparam in enumerate(plot_params):
    #     xlims[j] = {}
    #     for plot_data in dataset:
    ylims = {}
    xlower = {}
    xupper = {}

    for iset, plot_data in enumerate(dataset):
        
        data = plot_data["data"]
        label = plot_data.get("label", "")
        color = plot_data.get("color", colors[0])

        if color in COLOR_SHADES.keys():
            light = COLOR_SHADES.get(color)[0]
            cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", [light, COLOR_SHADES.get(color)[-1]])
        else:
            light = "{0}CC".format(color)
            cmap = matplotlib.colors.LinearSegmentedColormap.from_list("", [light, color])
        
        for i, yparam in enumerate(plot_params):

            for j, xparam in enumerate(plot_params):
                ax = axes[i, j]

                if j > i:
                    ax.set_visible(False)
                elif j == i:
                    # Plot diagonal
                    param = xparam # plot_params[i]
                    xdata = data[:, indices[param]]
                    kde = scipy.stats.gaussian_kde(xdata)
                    x = np.arange(np.min(xdata), np.max(xdata), np.abs(np.max(xdata)-np.min(xdata))/1000.)
                    y = kde(x)
                    p1, = ax.plot(x, y,color=color, label=label)

                    # Optional style: fill area under curves
                    if fill:
                        ax.fill_between(x,y, color=color, alpha=0.8)

                    ax.grid(True, alpha=0.5)
                    # ax.set_xlim(np.min(xdata), np.max(xdata))

                    if iset > 0:
                        if np.min(xdata) < xlower[i]:
                            xlower[i] = np.min(xdata)
                        if np.max(xdata) > xupper[i]:
                            xupper[i] = np.max(xdata)
                    else:
                        xlower[i] = np.min(xdata)
                        xupper[i] = np.max(xdata)

                    ylim = ax.get_ylim()
                    ymin = np.min(y)
                    if ylim[0] < ymin:
                        ylims[i] = ymin
                else:
                    # Plot 2D
                    # xparam = plot_params[j]
                    xdata = data[:, indices[xparam]]
                    # yparam = plot_params[i]
                    ydata = data[:, indices[yparam]]

                    k = scipy.stats.kde.gaussian_kde([xdata,ydata])
                    xi, yi = np.mgrid[xdata.min():xdata.max():nbins*1j, ydata.min():ydata.max():nbins*1j]
                    zi = k(np.vstack([xi.flatten(), yi.flatten()]))

                    ax.contour(xi, yi, zi.reshape(xi.shape), cmap=cmap)
                    ax.grid(True, alpha=0.5)

                # Set axlabels and ticks
                if j == 0 and i > 0:
                    ax.set_ylabel(ax_labels.get(yparam, yparam))
                else:
                    ax.set_yticklabels([])

                if i == N - 1:
                    ax.set_xlabel(ax_labels.get(xparam, xparam))
                else:
                    ax.set_xticklabels([])
                
                if i == 0 and j == 0 and plot_legend:
                    legend_handles.append(p1)
                    
    
    for i, x in enumerate(plot_params):
        ax = axes[i, i]
        ax.set_ylim(ylims[i], None) 
        ax.set_xlim(xlower[i], xupper[i]) 

    if plot_legend:
        legend_ax = axes[0, 0]
        pos = legend_ax.get_position()
        print(pos)
        fig.legend(handles=legend_handles, bbox_to_anchor=(pos.x1+0.05, 0.98), loc="upper left", fontsize=16)
                    
    
#         if len(dataset) > 0:
#             plt.legend(handles=[p1, p2], title='title', bbox_to_anchor=(1.05, 1), loc='upper left', fontsize='xx-small')

    plt.tight_layout()
    if outputfile:
        plt.savefig(outputfile)

    return fig

def get_bw_waveform(filename):
    """ 
    Reads in BayesWavePost output with confidence intervals
    Returns: timesamp,median_waveform, high_50, low_50, high_90, low_90 
    """
    d = np.genfromtxt(filename, delimiter=" ")
    return d[:, 0], d[:, 1], d[:, 2], d[:, 3], d[:, 4], d[:, 5]

def compute_signal_model_from_cbc_signal(cbc_data, sigcbc_data):
    """
    Computes signal model data from cbc data and sigcbc data (because BayesWavePost
    does not produce signal-only output files when running signal and cbc models).
    """
    data_types = ["median_waveform", "high_50", "low_50", "high_90", "low_90"]
    signal_data = {}
    
    for d in data_types:
        signal_data[d] = sigcbc_data[d] - cbc_data[d]
        
    return signal_data

def get_plot_data(postdir="", models=[], domains=["time", "frequency"], ifos=["H1"], dtype="whitened"):
    """
    Returns dictionary with relevant/requested data produced by BayesWavePost.
    This data can be used for plotting and further post-processing. 
    Example: 
    get_plot_data(postdir=postDir, models=["cbcsignal"], domain=["time", "freq"], ifos=["H1"], dtype="whitened")
    """
    post_data = {}
    
    for domain in domains:
        
        post_data[domain] = {}

        # post_data["times"] or post_data["freqs"]
        post_data["{0}s".format(domain[:4])] = np.genfromtxt(postdir + domain[:4] + "samp.dat") # freqsamp.dat or timesamp.dat
        
        for ifo in ifos:
            post_data[domain][ifo] = {}
            
            if domain == "time":
                post_data[domain][ifo]["time_data"] = np.genfromtxt("{0}{1}_data_{2}.dat".format(postdir, dtype, ifo))
                factor=1.0
            else:
                names = ['f','dpower','rpower','psd']
                post_data[domain][ifo]["frequency_data"] = np.recfromtxt("{0}gaussian_noise_model_{1}.dat".format(postdir, ifo),names=names)
                post_data[domain][ifo]["frequency_data"]["rpower"] = post_data[domain][ifo]["frequency_data"]["rpower"] 

            
            for model in models:
                # Time domain waveform with confidence intervals
                
                filename = postdir+'{1}/{1}_median_{2}_domain_waveform{3}_{0}.dat'.format(
                    ifo, model, domain, "" if domain == "time" else "_spectrum")
                try:
                    timesamp,median_waveform, high_50, low_50, high_90, low_90 = get_bw_waveform(filename)
                except:
                    print("  skipping model {0} for now".format(model))
                    continue
                
                
                post_data[domain][ifo][model] = {}
                post_data[domain][ifo][model]["samples"] = timesamp
                post_data[domain][ifo][model]["median_waveform"] = median_waveform
                post_data[domain][ifo][model]["high_50"] = high_50
                post_data[domain][ifo][model]["low_50"] = low_50
                post_data[domain][ifo][model]["high_90"] = high_90
                post_data[domain][ifo][model]["low_90"] = low_90
                
                print("  Got waveform data for {0} model: {1}".format(model, ifo))
            
            # Computed signal models from cbc and cbcsig models if requested
            if "signal" in models and not "signal" in post_data[domain][ifo].keys():
                if post_data[domain][ifo].get("cbc") and post_data[domain][ifo].get("cbcsignal"):
                    post_data[domain][ifo]["signal"] = compute_signal_model_from_cbc_signal(post_data[domain][ifo]["cbc"], post_data[domain][ifo]["cbcsignal"])      
                    post_data[domain][ifo]["signal"]["samples"] = timesamp
                    print("  computed signal model")
    
    # Parse trigtime
    ttrig = "/home/marcella.wijngaarden/Projects/BW+CBC/BNS_Injections/KastaunOhme/updated_test_runs/KO_BNS_cbcsig_16s_f4096Hz_5c_fixT_1M_6xDS/trigtime_1187008882.430000067_0.0_0.0_0/"
    ttrig = postdir.replace("/post/", "").replace("/post", "")
    ttrig = ttrig.split("/")[-1]
    ttrig = ttrig.split("_")[1]
    post_data["trigtime"] = ttrig

    return post_data


def make_time_domain_plot(postdir="", models=[], ifos=["H1"], plot_models=["cbc", "signal"], 
                          model_colors=MODEL_COLORS, 
                          model_labels=[{}], ifo_labels={},
                          data={}, multidata=[], plot_data=True,
                          xlims=[-0.05, 0.03], ylims=[], colours=[{}], linestyles=[{}],
                          figsize=(12,6), fig=None, ax=None, legend=True,
                         outputfile=None, axlabel_fsize=None, xlabel=""):
    """
    Loads relevant files from BayesWavePost directory if path is given. 
    Otherwise, uses given data structures.
    """
    
    plt.rc('text', usetex=True)
    
    if not data and not multidata:
        data = get_plot_data(postdir=postdir)
    
    if not colours:
        colours = MODEL_COLORS
    
    # In case we are plotting data for a single run directory
    # (Note, a single directory can still plot multiple models)
    if not multidata:
        multidata = [data]
        models = [models]
        model_labels = [model_labels] 
        colours = [colours]
        linestyles = [linestyles]
        
    iplots = len(ifos)
    fig, axes = plt.subplots(iplots, 1, figsize=figsize, sharex=True)

    # Set defaults
    if not axlabel_fsize:
        axlabel_fsize = matplotlib.rcParams['xtick.labelsize']
    
    
    for irun, data in enumerate(multidata):
        
        
        for i, ifo in enumerate(ifos):
            if iplots > 1:
                ax = axes[i]
            else:
                ax = axes
            
            if irun == 0 and plot_data:
                ax.plot(data["times"], data["time"][ifo]["time_data"], color="silver", ls='-')

            for model in models[irun]:
                model_data = data["time"][ifo][model]
                model_label = model_labels[irun].get(model, model)
                colour = colours[irun].get(model)

                ax.fill_between(data["times"], model_data["low_50"], model_data["high_50"], facecolor=colour, edgecolor=colour, alpha=0.5, ls=linestyles[irun].get(model, "-"))
                ax.fill_between(data["times"], model_data["low_90"], model_data["high_90"], facecolor=colour, edgecolor=colour, alpha=0.3, ls=linestyles[irun].get(model, "-"))

                ax.plot(data["times"], model_data["median_waveform"], color=colour, linewidth=1, alpha=1, label=model_label, ls=linestyles[irun].get(model, "-"))
            

            if ylims:
                ax.set_ylim(ylims[0], ylims[-1])
                    
            ax.set_xlim(xlims[0], xlims[-1])

            # Only show xlabel on last ax
            if i == iplots - 1:
                if not xlabel:
                    ax.set_xlabel("Time relative to {0} (s)".format(data["trigtime"]), fontsize=axlabel_fsize)
                else:
                    ax.set_xlabel(xlabel, fontsize=axlabel_fsize)

            # Only show legend on first ax
            if legend and i == 0:
                ax.legend(loc="upper left")

            ax.text(0.05, 0.05, ifo_labels.get(ifo, ifo), transform=ax.transAxes, fontsize=axlabel_fsize+5)
            ax.set_ylabel(r"$\sigma_{noise}$", fontsize=axlabel_fsize+8)
    

        
    plt.tight_layout()
    
    if outputfile:
        plt.savefig(outputfile, dpi=150)
        
    return fig

def make_frequency_domain_plot(postdir="", models=[], ifos=["H1"], plot_models=["cbc", "signal"], 
                          model_colors=MODEL_COLORS, 
                          model_labels=[{}], ifo_labels={},
                          data={}, multidata=[], plot_data=True,
                          xlims=[1000, 4096], ylims=[], colours=[{}], linestyles=[{}],
                          figsize=(12,6), fig=None, ax=None, legend=True,
                         outputfile=None, axlabel_fsize=None, psd=False, asd=False,
                         plot_ifo_label=True, ylabel=""):
    """
    Loads relevant files from BayesWavePost directory if path is given. 
    Otherwise, uses given data structures.
    """
    
    plt.rc('text', usetex=True)
    
    if not data and not multidata:
        data = get_plot_data(postdir=postdir)
    
    if not colours:
        colours = MODEL_COLORS
    
    # In case we are plotting data for a single run directory
    # (Note, a single directory can still plot multiple models)
    if not multidata:
        multidata = [data]
        models = [models]
        model_labels = [model_labels] 
        colours = [colours]
        linestyles = [linestyles]
        
    iplots = len(ifos)
    fig, axes = plt.subplots(1, iplots, figsize=figsize, sharex=True)

    # Set defaults
    if not axlabel_fsize:
        axlabel_fsize = matplotlib.rcParams['xtick.labelsize']
    
    
    for irun, data in enumerate(multidata):
        
        
        for i, ifo in enumerate(ifos):
            if iplots > 1:
                ax = axes[i]
            else:
                ax = axes

            freqs = data["frequency"][ifo]["frequency_data"]["f"]
            # freqs = data["freqs"]
            df = data["frequency"][ifo]["frequency_data"]["f"][2] - data["frequency"][ifo]["frequency_data"]["f"][1]
            if irun == 0:
                
                if psd:
                    ax.plot(freqs, data["frequency"][ifo]["frequency_data"]["psd"]*2*df, color="k", ls='--')
                    sq_factor = 1.0
                    factor = 1.0
                elif asd:
                    ax.plot(freqs, np.sqrt(data["frequency"][ifo]["frequency_data"]["psd"]*2*df), color="k", ls='--')
                    sq_factor = 0.5
                    factor = 2.0*np.sqrt(data["frequency"][ifo]["frequency_data"]["f"])


            
                if plot_data:
                    ax.plot(freqs, factor*data["frequency"][ifo]["frequency_data"]["rpower"]**sq_factor, color="silver", ls='-')
            
            for model in models[irun]:
                model_data = data["frequency"][ifo][model]
                model_label = model_labels[irun].get(model, model)
                colour = colours[irun].get(model)

                if asd:
                    factor = 2.0*np.sqrt(model_data["samples"])

                ax.fill_between(model_data["samples"], factor*np.abs(model_data["low_50"]**sq_factor), factor*np.abs(model_data["high_50"])**sq_factor, facecolor=colour, edgecolor=colour, alpha=0.5, ls=linestyles[irun].get(model, "-"))
                ax.fill_between(model_data["samples"], factor*np.abs(model_data["low_90"])**sq_factor, factor*np.abs(model_data["high_90"])**sq_factor, facecolor=colour, edgecolor=colour, alpha=0.3, ls=linestyles[irun].get(model, "-"))

                ax.plot(model_data["samples"], factor*np.abs(model_data["median_waveform"])**sq_factor, color=colour, linewidth=1, alpha=1, label=model_label, ls=linestyles[irun].get(model, "-"))
            
            if xlims:
                ax.set_xlim(xlims[0], xlims[-1])
            
            if ylims:
                 ax.set_ylim(ylims[0], ylims[-1])
                
            # Only show xlabel on last ax
            if i == iplots - 1:
                ax.set_xlabel("Frequency (Hz)", fontsize=axlabel_fsize)

            # Only show legend on first ax
            if legend and i == 0:
                ax.legend(loc="upper left")

            # Plot ifo label text in frame
            if plot_ifo_label:
                ax.text(0.05, 0.05, ifo_labels.get(ifo, ifo), transform=ax.transAxes, fontsize=axlabel_fsize+5)

            # if asd:
            #     ylabel_txt = r"2|h(f)| $\sqrt{f}$ & $\sqrt{S_n(f)}$"
            # else:
            #     ylabel_txt = r"$\sigma_{noise}$"
            ylabel_txt = ""
            if ylabel:
                ylabel = ylabel

            ax.set_ylabel(ylabel, fontsize=axlabel_fsize) # fontsize=axlabel_fsize+8)
    
            ax.set_yscale("log")
#             ax.set_xscale("log")
        
    plt.tight_layout()
    
    if outputfile:
        plt.savefig(outputfile, dpi=150)
        
    return fig

# def make_time_domain_plot(postdir="", models=[], ifos=["H1"], plot_models=["cbc", "signal"], 
#                           model_colors=MODEL_COLORS, 
#                           model_labels={}, ifo_labels={},
#                           data={}, 
#                           xlims=[-0.05, 0.03], 
#                           figsize=(12,6), fig=None, ax=None, legend=True,
#                          outputfile=None, axlabel_fsize=None):
#     """
#     Loads relevant files from BayesWavePost directory if path is given. 
#     Otherwise, uses given data structures.
#     """
    
#     plt.rc('text', usetex=True)
    
#     if not data:
#         data = get_plot_data(postdir=postdir)
    
#     iplots = len(ifos)
#     fig, axes = plt.subplots(iplots, 1, figsize=figsize, sharex=True)

#     # Set defaults
#     if not axlabel_fsize:
#         axlabel_fsize = matplotlib.rcParams['xtick.labelsize']
    
#     for i, ifo in enumerate(ifos):
#         ax = axes[i]
        
#         ax.plot(data["times"], data[ifo]["time_data"], color="silver", ls='-')

#         for model in models:
#             model_data = data[ifo][model]
#             model_label = model_labels.get(model, model)
#             colour = MODEL_COLORS.get(model)

#             ax.fill_between(data["times"], model_data["low_50"], model_data["high_50"], facecolor=colour, edgecolor=colour, alpha=0.5)
#             ax.fill_between(data["times"], model_data["low_90"], model_data["high_90"], facecolor=colour, edgecolor=colour, alpha=0.3)

#             ax.plot(data["times"], model_data["median_waveform"], color=colour, linewidth=1, alpha=1, label=model_label)
#             ax.set_xlim(xlims[0], xlims[-1])
        
#         # Only show xlabel on last ax
#         if i == iplots - 1:
#             ax.set_xlabel("Time (s)", fontsize=axlabel_fsize)
        
#         # Only show legend on first ax
#         if legend and i == 0:
#             ax.legend()
            
#         ax.text(0.05, 0.05, ifo_labels.get(ifo, ifo), transform=ax.transAxes, fontsize=axlabel_fsize+5)
#         ax.set_ylabel(r"$\sigma_{noise}$", fontsize=axlabel_fsize+8)
    

        
#     plt.tight_layout()
    
#     if outputfile:
#         plt.savefig(outputfile, dpi=150)
        
#     return fig
